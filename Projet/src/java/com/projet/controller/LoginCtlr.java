/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.controller;

import com.projet.beans.Employe;
import com.projet.beans.User;
import com.projet.dao.EmployeDAO;
import com.projet.dao.UserDAO;
import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


/**
 *
 * @author alexandre
 */
public class LoginCtlr extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    
    private UserDAO userDAO;
    private User user;
    private ArrayList<Employe> employeList;
    private EmployeDAO employeDAO;
    private boolean authenticated;
    
    @Override
    public void init() throws ServletException {
        userDAO = new UserDAO();
        employeDAO = new EmployeDAO();
        employeList = new ArrayList<>();
        authenticated = false;
    }
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        HttpSession session = request.getSession();
        session.setAttribute("authenticated", authenticated);
        
        if(!(boolean)session.getAttribute("authenticated")) {
            authentificationProcess(request, response); 
        }
        
        if (request.getParameter("ajouter") != null) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/EmployeEditPage.jsp").forward(request, response);
            return;
        }

        if (request.getParameter("detail") != null && request.getParameter("employeSelected") != null) {
            this.getServletContext().getRequestDispatcher("/WEB-INF/EmployeEditPage.jsp").forward(request, response);
            return;
        }
        
        refreshScreen(session);
        this.getServletContext().getRequestDispatcher("/WEB-INF/ManagementPage.jsp").forward(request, response);
    }
    
    private void authentificationProcess(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        user = new User(request.getParameter("login"), request.getParameter("password"));  
        HttpSession session = request.getSession();
        
        if (request.getParameter("login") == null) {
            this.getServletContext().getRequestDispatcher( "/WEB-INF/LoginPage.jsp" ).forward( request, response );
            return;
        }
        
        if ("".equals(request.getParameter("login")) || "".equals(request.getParameter("password"))) {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            this.getServletContext().getRequestDispatcher( "/WEB-INF/LoginPage.jsp" ).forward( request, response );
            return;
        }
        
        if(!isAuthenticated(user)) {
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            this.getServletContext().getRequestDispatcher( "/WEB-INF/LoginPage.jsp" ).forward( request, response );
            return;
        }
        authenticated = true;
    }
    
    private void refreshScreen(HttpSession session) {
        employeList = employeDAO.findAll();
        session.setAttribute("employeList", employeList);
    }
    
    private boolean isAuthenticated(User user) {
        return userDAO.search(user);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        session.setAttribute("authenticated", authenticated);
        
        //AUTH
        if(!(boolean)session.getAttribute("authenticated")) {
            authentificationProcess(request, response); 
        }

        //DELETE CASE
        if (request.getParameter("supprimer") != null && request.getParameter("employeSelected") != null) {
            if (employeDAO.delete(employeList.get(Integer.parseInt(request.getParameter("employeSelected"))).getId())) {
                response.setStatus(HttpServletResponse.SC_ACCEPTED);
            } else {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
            }
            refreshScreen(session);
            this.getServletContext().getRequestDispatcher("/WEB-INF/ManagementPage.jsp").forward(request, response);
            return;
            
        }
        //UPDATE CASE
        if ("Modifier".equals(request.getParameter("action"))) {
            Employe employe = new Employe(
                    Integer.parseInt(request.getParameter("id")),
                    request.getParameter("nom"),
                    request.getParameter("prenom"),
                    request.getParameter("telDomicile"),
                    request.getParameter("telPortable"),
                    request.getParameter("telPro"),
                    request.getParameter("adresse"),
                    request.getParameter("codePostal"),
                    request.getParameter("ville"),
                    request.getParameter("mail"));
            employeDAO.update(employe);
            refreshScreen(session);
            this.getServletContext().getRequestDispatcher("/WEB-INF/ManagementPage.jsp").forward(request, response);
            return;
        } 
        
        //INSERT CASE
        if ("Ajouter".equals(request.getParameter("action"))) {
            Employe employe = new Employe(
                    request.getParameter("nom"),
                    request.getParameter("prenom"),
                    request.getParameter("telDomicile"),
                    request.getParameter("telPortable"),
                    request.getParameter("telPro"),
                    request.getParameter("adresse"),
                    request.getParameter("codePostal"),
                    request.getParameter("ville"),
                    request.getParameter("mail"));

            if (!employeDAO.create(employe)) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                this.getServletContext().getRequestDispatcher("/WEB-INF/EmployeEditPage.jsp").forward(request, response);
                return;
            }
        }
        
        if (request.getParameter("backToLogin") != null) {
            authenticated = false;
            session.setAttribute("authenticated", false);
            this.getServletContext().getRequestDispatcher("/WEB-INF/ByeByePage.jsp").forward(request, response);
        }
        
        refreshScreen(session);
        this.getServletContext().getRequestDispatcher("/WEB-INF/ManagementPage.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
