/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.dao;

import com.projet.beans.Employe;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexandre
 */
public class EmployeDAO extends DAO<Employe> {

    public EmployeDAO() {
        super(DatabaseConnection.getInstance());
    }

    @Override
    public boolean create(Employe obj) {
        Connection dbConnection = DatabaseConnection.getInstance();
        Statement statement;
        
        try {
            statement = dbConnection.createStatement();
            String requete = "INSERT INTO EMPLOYE (PRENOM,NOM,TELDOM,TELPORT,TELPRO,ADRESSE,CODEPOSTAL,VILLE,EMAIL) VALUES ("
                    + "'"+obj.getPrenom()+"', "
                    + "'"+obj.getNom()+"', "
                    + "'"+obj.getTelDomicile()+"', "
                    + "'"+obj.getTelPortable()+"', "
                    + "'"+obj.getTelPro()+"', "
                    + "'"+obj.getAdresse()+"', "
                    + "'"+obj.getCodePostal()+"', "
                    + "'"+obj.getVille()+"', "
                    + "'"+obj.getMail()+"');";
            statement.executeUpdate(requete);
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
            return false;
        }
        return true;
    }

    @Override
    public boolean delete(Employe obj) {
        Connection dbConnection = DatabaseConnection.getInstance();
        Statement statement;
        
        try {
            statement = dbConnection.createStatement();
            String requete = "DELETE FROM EMPLOYE WHERE PRENOM = '"+obj.getPrenom()+"' AND NOM = '"+obj.getNom()+"';";
                    
            statement.executeUpdate(requete);
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
        }
        return true;
    }
    
    public boolean delete(int id) {
        Connection dbConnection = DatabaseConnection.getInstance();
        Statement statement;

        try {
            statement = dbConnection.createStatement();
            String requete = "DELETE FROM EMPLOYE WHERE ID = '" +id+ "';";

            statement.executeUpdate(requete);
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
            return false;
        }
        return true;
    }

    @Override
    public boolean update(Employe obj) {
        Connection dbConnection = DatabaseConnection.getInstance();
        Statement statement;
        
        try {
            statement = dbConnection.createStatement();
            String requete = ""
                    + "UPDATE EMPLOYE SET "
                    + "PRENOM='"+obj.getPrenom()+"', "
                    + "NOM='"+obj.getNom()+"', "
                    + "TELDOM='"+obj.getTelDomicile()+"', "
                    + "TELPORT='"+obj.getTelPortable()+"', "
                    + "TELPRO='"+obj.getTelPro()+"', "
                    + "ADRESSE='"+obj.getAdresse()+"', "
                    + "CODEPOSTAL='"+obj.getCodePostal()+"', "
                    + "VILLE='"+obj.getVille()+"', "
                    + "EMAIL='"+obj.getMail()+"' "
                    + "WHERE ID = "+obj.getId()+";";
                    
            statement.executeUpdate(requete);
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
            return false;
        }
        return true;
    }

    @Override
    public Employe find(Employe obj) {
        Connection dbConnection = DatabaseConnection.getInstance();
        Employe employe = new Employe();
        Statement statement;
        ResultSet result;
        
        try {
            statement = dbConnection.createStatement();
            String requete = "SELECT * FROM EMPLOYE WHERE PRENOM='"+obj.getPrenom()+"' AND NOM='"+obj.getNom()+"';";
            result = statement.executeQuery(requete);
            while (result.next()) {
                employe.setId(result.getInt("ID"));
                employe.setPrenom(result.getString("PRENOM"));
                employe.setNom(result.getString("NOM"));
                employe.setTelDomicile(result.getString("TELDOM"));
                employe.setTelPortable(result.getString("TELPORT"));
                employe.setTelPro(result.getString("TELPRO"));
                employe.setAdresse(result.getString("ADRESSE"));
                employe.setCodePostal(result.getString("CODEPOSTAL"));
                employe.setVille(result.getString("VILLE"));
                employe.setMail(result.getString("EMAIL"));
            }
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
            return null;
        }
        return employe;
    }
    
        public Employe find(int id) {
            Connection dbConnection = DatabaseConnection.getInstance();
            Employe employe = new Employe();
            Statement statement;
            ResultSet result;

            try {
                statement = dbConnection.createStatement();
                String requete = "SELECT * FROM EMPLOYE WHERE ID='"+id+"';";
                result = statement.executeQuery(requete);
                while (result.next()) {
                    employe.setId(result.getInt("ID"));
                    employe.setPrenom(result.getString("PRENOM"));
                    employe.setNom(result.getString("NOM"));
                    employe.setTelDomicile(result.getString("TELDOM"));
                    employe.setTelPortable(result.getString("TELPORT"));
                    employe.setTelPro(result.getString("TELPRO"));
                    employe.setAdresse(result.getString("ADRESSE"));
                    employe.setCodePostal(result.getString("CODEPOSTAL"));
                    employe.setVille(result.getString("VILLE"));
                    employe.setMail(result.getString("EMAIL"));
                }
            } catch (SQLException WrongSQLRequestException) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
                return null;
            }
            return employe;
    }
        
        public ArrayList<Employe> findAll() {
            Connection dbConnection = DatabaseConnection.getInstance();
            ArrayList<Employe> employeList = new ArrayList<>();
            Statement statement;
            ResultSet result;
            int i = 0;

            try {
                statement = dbConnection.createStatement();
                String requete = "SELECT * FROM EMPLOYE;";
                result = statement.executeQuery(requete);
                while (result.next()) {
                    employeList.add(new Employe()); 
                    employeList.get(i).setId(result.getInt("ID"));
                    employeList.get(i).setPrenom(result.getString("PRENOM"));
                    employeList.get(i).setNom(result.getString("NOM"));
                    employeList.get(i).setTelDomicile(result.getString("TELDOM"));
                    employeList.get(i).setTelPortable(result.getString("TELPORT"));
                    employeList.get(i).setTelPro(result.getString("TELPRO"));
                    employeList.get(i).setAdresse(result.getString("ADRESSE"));
                    employeList.get(i).setCodePostal(result.getString("CODEPOSTAL"));
                    employeList.get(i).setVille(result.getString("VILLE"));
                    employeList.get(i).setMail(result.getString("EMAIL"));
                    i++;
                }
            } catch (SQLException WrongSQLRequestException) {
                Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
                return null;
            }
            return employeList;
        }
    
}
