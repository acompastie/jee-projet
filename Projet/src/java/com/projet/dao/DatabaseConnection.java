/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.dao;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexandre
 */
public class DatabaseConnection {

    private static Connection connect;
    private String urlDb;
    private String userDb;
    private String psswdDb;

    private DatabaseConnection() {
        Properties prop = new Properties();
        InputStream input;
        ClassLoader d = Thread.currentThread().getContextClassLoader();
        try {
            DriverManager.registerDriver(new com.mysql.cj.jdbc.Driver ());
        } catch (SQLException ex) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            input = d.getResourceAsStream("com/projet/properties/db.properties");
            prop.load(input);
        } catch (IOException FileNotFoundException) {
            Logger.getLogger(DatabaseConnection.class.getName()).log(Level.SEVERE, null, FileNotFoundException);
        }

        urlDb = prop.getProperty("urlDb");
        userDb = prop.getProperty("userDb");
        psswdDb = prop.getProperty("psswdDb");

        try {
            connect = DriverManager.getConnection(urlDb, userDb, psswdDb);
        } catch (SQLException invalidCredentialsException) {
            invalidCredentialsException.printStackTrace();
        }
    }

    public static Connection getInstance() {
        if (connect == null) {
            new DatabaseConnection();
        }
        return connect;
    }

}
