/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.projet.dao;

import com.projet.beans.User;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author alexandre
 */
public class UserDAO extends DAO<User> {

    public UserDAO() {
        super(DatabaseConnection.getInstance());
    }

    @Override
    public boolean create(User obj) {
        throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean delete(User obj) {
        throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean update(User obj) {
        throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public User find(User obj) {
        throw new UnsupportedOperationException("Not supported."); //To change body of generated methods, choose Tools | Templates.
    }

    public boolean search(User obj) {
        Connection dbConnection = DatabaseConnection.getInstance();
        User userFind = new User();
        Statement statement;
        ResultSet result;
        
        try {
            statement = dbConnection.createStatement();
            String requete = "SELECT * FROM USER WHERE LOGIN='"+obj.getLogin()+"' AND PASSWORD='"+obj.getPassword()+"';";
            result = statement.executeQuery(requete);
            
            while (result.next()) {
                userFind.setLogin(result.getString("LOGIN"));
                userFind.setPassword(result.getString("PASSWORD"));
            }
        } catch (SQLException WrongSQLRequestException) {
            Logger.getLogger(UserDAO.class.getName()).log(Level.SEVERE, null, WrongSQLRequestException);
        }  
        return userFind.equals(obj);
    }
}