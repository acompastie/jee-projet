package com.projet.beans;

import java.util.Objects;

public class Employe {

    private int id;
    private String nom;
    private String prenom;
    private String telPortable;
    private String telDomicile;
    private String telPro;
    private String adresse;
    private String ville;
    private String codePostal;
    private String mail;

    public Employe(String nom, String prenom, String telPortable, String telDomicile, String telPro, String adresse, String ville, String codePostal, String mail) {
        this.nom = nom;
        this.prenom = prenom;
        this.telPortable = telPortable;
        this.telDomicile = telDomicile;
        this.telPro = telPro;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostal;
        this.mail = mail;
    }

    
    public Employe(int id, String nom, String prenom, String telPortable, String telDomicile, String telPro, String adresse, String ville, String codePostal, String mail) {
        this.id = id;
        this.nom = nom;
        this.prenom = prenom;
        this.telPortable = telPortable;
        this.telDomicile = telDomicile;
        this.telPro = telPro;
        this.adresse = adresse;
        this.ville = ville;
        this.codePostal = codePostal;
        this.mail = mail;
    }

    public Employe() {
        id = 0;
        nom = prenom = telPortable = telDomicile = telPro = adresse = ville = codePostal = mail =  "";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getTelPortable() {
        return telPortable;
    }

    public void setTelPortable(String telPortable) {
        this.telPortable = telPortable;
    }

    public String getTelDomicile() {
        return telDomicile;
    }

    public void setTelDomicile(String telDomicile) {
        this.telDomicile = telDomicile;
    }

    public String getTelPro() {
        return telPro;
    }

    public void setTelPro(String telPro) {
        this.telPro = telPro;
    }

    public String getVille() {
        return ville;
    }

    public void setVille(String ville) {
        this.ville = ville;
    }

    public String getCodePostal() {
        return codePostal;
    }

    public void setCodePostal(String codePostal) {
        this.codePostal = codePostal;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
    
    public String getAdresse() {
        return adresse;
    }

    public void setAdresse(String adresse) {
        this.adresse = adresse;
    }
    @Override
    public int hashCode() {
        int hash = 7;
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Employe other = (Employe) obj;
        if (this.id != other.id) {
            return false;
        }
        if (!Objects.equals(this.nom, other.nom)) {
            return false;
        }
        if (!Objects.equals(this.prenom, other.prenom)) {
            return false;
        }
        if (!Objects.equals(this.telPortable, other.telPortable)) {
            return false;
        }
        if (!Objects.equals(this.telDomicile, other.telDomicile)) {
            return false;
        }
        if (!Objects.equals(this.telPro, other.telPro)) {
            return false;
        }
        if (!Objects.equals(this.ville, other.ville)) {
            return false;
        }
        if (!Objects.equals(this.codePostal, other.codePostal)) {
            return false;
        }
        if (!Objects.equals(this.mail, other.mail)) {
            return false;
        }
        return true;
    }
}
