<%-- 
    Document   : ManagementPage
    Created on : 24 nov. 2018, 11:36:35
    Author     : alexandre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Liste des employés</title>
            <form action="/Projet/LoginCtlr" method="post">
        <nav class="navbar navbar-expand-lg navbar-light" 
             style="background-color: #77B5FE">
            <div style="margin:0 0 0 75%">
                <input hidden name="backToLogin" value="true">
                Votre connexion est active   
                <input type="image" src="../../logout.png" >
            </div>
        </nav>
    </form>
    </head>
    <body>

        <div>
                        <h1 style="margin-left: 120px">Liste des employés</h1>

            <form action="/Projet/LoginCtlr"
                  style="margin:20px 0 100px 50px;
                  text-align:center;
                  border-width:10px;
                  border-color:black;
                  width:100%;
                  position:absolute;
                  left:100px">

                <div style="margin:10px 0;
                     width:50px">
                    <table class="table table-hover table-sm"
                           style="margin:7% 0% 0 20%;
                           text-align:center">
                        <thead class="thead-light">
                            <tr>
                                <th scope="col"> </th>
                                <th scope="col">Id</th>
                                <th scope="col">NOM</th>
                                <th scope="col">PRENOM</th>
                                <th scope="col">TEL DOMICILE</th>
                                <th scope="col">TEL PORTABLE</th>
                                <th scope="col">TEL PRO</th>
                                <th scope="col">ADRESSE</th>
                                <th scope="col">CODE POSTAL</th>
                                <th scope="col">VILLE</th>
                                <th scope="col">EMAIL</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <c:forEach items="${employeList}" var="employe" varStatus="loop">
                                <tr>
                                    <td>
                                        <div>
                                            <input type="radio" id="${employe.id}" name="employeSelected" value="${loop.index}">
                                        </div>
                                    </td>
                                    <td scope="row"><c:out value="${employe.id}"/></td>
                                    <td><c:out value="${employe.nom}"/></td>
                                    <td><c:out value="${employe.prenom}"/></td>
                                    <td><c:out value="${employe.telDomicile}"/></td>
                                    <td><c:out value="${employe.telPortable}"/></td>
                                    <td><c:out value="${employe.telPro}"/></td>
                                    <td><c:out value="${employe.adresse}"/></td>
                                    <td><c:out value="${employe.codePostal}"/></td>
                                    <td><c:out value="${employe.ville}"/></td>
                                    <td><c:out value="${employe.mail}"/></td>
                                </tr>
                            </c:forEach>
                            </tr>
                        </tbody>
                    </table>
                </div>

                <input type="submit" class="btn btn-outline-secondary" name="ajouter" value="Ajouter" />
                <input type="submit" class="btn btn-outline-secondary" name="detail" value="Détail">
                <input type="submit" class="btn btn-outline-secondary" name="supprimer" formmethod="post" value="Supprimer">
            </form>
        </div>
        <c:if test="${employeList.isEmpty()}">
            <p style="color:Tomato;">Nous devons recruter !</p>
        </c:if>
        <c:if test="${pageContext.response.status == 202}">
            <p style="color:DodgerBlue;text-align: center">
                La suppression a réussi !
            </p>
        </c:if>
        <c:if test="${pageContext.response.status == 404}">
            <p style="color:Tomato;text-align: center">
                La suppression a échouée !
            </p>
        </c:if>
    </body>
</html>
