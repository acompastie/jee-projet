<%-- 
    Document   : EmployeEditPage
    Created on : 25 nov. 2018, 14:30:59
    Author     : alexandre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Ajouter un employé</title>
    <form action="/Projet/LoginCtlr" method="post">
        <nav class="navbar navbar-expand-lg navbar-light" 
             style="background-color: #77B5FE">
            <div style="margin:0 0 0 75%">
                <input hidden name="backToLogin" value="true">
                Votre connexion est active   
                <input type="image" src="../../logout.png" >
            </div>
        </nav>
    </form>
</head>

<body>
    <c:if test="${param.ajouter == 'Ajouter' or (empty param.ajouter and empty param.detail)}">
        <div class="form-group">
            <h1 style="margin-left: 120px">Ajouter un employé</h1>
            <form>
                <table style="margin:0% 25% 0 25%">
                    <tr>
                        <td>Nom</td>
                        <td colspan="3"><input type="text" class="form-control" name="nom" required></td> <br>
                    </tr>
                    <tr>
                        <td>Prenom</td>
                        <td colspan="3"><input type="text" class="form-control" name="prenom" required></td>
                    </tr>
                    <tr>
                        <td>Tel domicile</td>
                        <td colspan="3"><input type="tel" class="form-control" name="telDomicile" required></td>
                    </tr>
                    <tr>
                        <td>Tel portable</td>
                        <td colspan="3"><input type="tel" class="form-control" name="telPortable" required></td>
                    </tr>
                    <tr>
                        <td>Tel pro</td>
                        <td colspan="3"><input type="tel" class="form-control" name="telPro" required></td>
                    </tr>
                    <tr>
                        <td>Adresse</td>
                        <td><input type="text" class="form-control" name="adresse" required></td>
                        <td>Code postal</td>
                        <td><input type="text" class="form-control" name="codePostal" required></td>
                    </tr>
                    <tr>
                        <td>Ville</td>
                        <td><input type="text" class="form-control" name="ville" required></td><br>
                    <td>E-Mail</td>
                    <td><input type="email" class="form-control" name="mail" required></td><br>
                    </tr>
                </table>
                <input style="margin-left:40%" type="submit" class="btn btn-primary" name="action" value="Ajouter">
            </form> 
        </div>
    </c:if>

    <c:if test="${param.detail == 'Detail'}">
        <form action="/Projet/LoginCtlr" method="post">
            <table>
                <tr>
                    <td>Nom</td>
                    <td><input type="text" name="nom" value="${employeList[param.employeSelected].getNom()}"></td>
                    <td>ID</td>
                    <td><input type="text" name="id" value="${employeList[param.employeSelected].getId()}"></td>
                <input name="employeSelected" value="${param.employeSelected}" hidden>

                </tr>
                <tr>
                    <td>Prenom</td>
                    <td><input type="text" name="prenom" value="${employeList[param.employeSelected].getPrenom()}"></td>
                </tr>
                <tr>
                    <td>Tel domicile</td>
                    <td><input type="text" name="telDomicile" value="${employeList[param.employeSelected].getTelDomicile()}">
                </tr>
                <tr>
                    <td>Tel portable</td>
                    <td><input type="text" name="telPortable" value="${employeList[param.employeSelected].getTelPortable()}">
                </tr>
                <tr>
                    <td>Tel pro</td>
                    <td><input type="text" name="telPro" value="${employeList[param.employeSelected].getTelPro()}"></td>
                </tr>
                <tr>
                    <td>Adresse</td>
                    <td><input type="text" name="adresse" value="${employeList[param.employeSelected].getAdresse()}"></td>
                    <td>Code postal</td>
                    <td><input type="text" name="codePostal" value="${employeList[param.employeSelected].getCodePostal()}"></td>
                </tr>
                <tr>
                    <td>Ville</td>
                    <td><input type="text" name="ville" value="${employeList[param.employeSelected].getVille()}"></td>
                    <td>E-Mail</td>
                    <td><input type="text" name="mail" value="${employeList[param.employeSelected].getMail()}"></td>
                </tr>
            </table>
            <br>
            <input type="submit" name="action" value="Modifier">
        </form>

    </c:if>
    <form action="/Projet/LoginCtlr">
        NOM
        <input style="margin-left:40%" type="submit" class="btn btn-outline-secondary" value="Voir la liste">
    </form>
    <c:if test="${pageContext.response.status == 404}"><p style="color:Tomato;">L'ajout a échoué !</p></c:if>
</body>
</html>
