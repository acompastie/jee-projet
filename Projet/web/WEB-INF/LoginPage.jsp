<%-- 
    Document   : LoginPage
    Created on : 24 nov. 2018, 11:10:40
    Author     : alexandre
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login</title>
    </head>
    <body>
        <c:if test="${pageContext.response.status == 401}">
            <p style="color:Tomato;text-align: center">Echec de la connexion ! 
                Verifiez votre login et/ou mot de passe et réessayez.
            </p>
        </c:if>
        <c:if test="${pageContext.response.status == 400}">
            <p style="color:Tomato;text-align: center">
                Vous devez renseigner les deux champs.
            </p>
        </c:if>
        <!--haut droit bas gauche-->
        <h1 style="margin:3% 0% 0 0%;
            text-align: center;
            backgroung-color:blue">CONNEXION</h1>

        <form action="/Projet/LoginCtlr" method="post" 
              style="margin:6% 35% 0 35%;
              padding:2% 3% 2% 3%;
              border-width:1px;
              border-style:solid;
              border-color:#77B5FE;
              border-radius:7px">

            Identifiant<br>
            <input type="text" class="form-control" name="login"autofocus><br>
            Mot de passe<br>
            <input type="password" class="form-control" name="password"><br>
            <input  type="submit" class="btn btn-primary" value="Envoyer">
        </form>
    </body>
</html>

