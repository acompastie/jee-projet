# Configuration du Projet

Pour lancer la bdd MySQL on utilise docker : 

## Execution

```
docker run --name mysql -e MYSQL_DATABASE=PROJET -e MYSQL_ROOT_PASSWORD=root -e MYSQL_USER=adm -e MYSQL_PASSWORD=adm -p 3306:3306 -d mysql:latest
```

## Credentials

Dans l'IDE on renseigne les champs ainsi : 

Driver : Celui de MySQL

```
jdbc:mysql://localhost:3306/PROJET
```

User & psswd & DB :

```
USER=root 
PSSWD=root
DATABASE=PROJET
```

## Remplir la BDD

Script --> Disponible dans WEB-INF, a exécuter avant de lancer l'application.
```
/*Création de la table EMPLOYES*/
CREATE TABLE USER (
	ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	LOGIN VARCHAR(25) NOT NULL,
	PASSWORD VARCHAR(25) NOT NULL
);

CREATE TABLE EMPLOYE (
	ID INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	NOM VARCHAR(25) NOT NULL,
	PRENOM VARCHAR(25) NOT NULL,
	TELDOM VARCHAR(10) NOT NULL,
	TELPORT VARCHAR(10) NOT NULL,
	TELPRO VARCHAR(10) NOT NULL,
	ADRESSE VARCHAR(150) NOT NULL,
	CODEPOSTAL VARCHAR(5) NOT NULL,
	VILLE VARCHAR(25) NOT NULL,
	EMAIL VARCHAR(25) NOT NULL
);

/*Insertion de 4 membres*/
INSERT INTO EMPLOYE(NOM,PRENOM,TELDOM,TELPORT,TELPRO,ADRESSE,CODEPOSTAL,VILLE,EMAIL) VALUES
('Turing','Alan','0123456789','0612345678','0698765432','2 rue des Automates','92700','Colombes','aturing@efrei.fr'),
('Galois','Evariste','0145362787','0645362718','0611563477','21 rue des Morts-trop-jeunes','92270','Bois-colombes','egalois@efrei.fr'),
('Boole','George','0187665987','0623334256','0654778654','65 rue de la Logique','92700','Colombes','gboole@efrei.fr'),
('Gauss','Carl Friedrich','0187611987','0783334256','0658878654','6 rue des Transformations','75016','Paris','cgauss@efrei.fr'),
('Pascal','Blaise','0187384987','0622494256','0674178654','39 bvd de Port-Royal','21000','Dijon','bpascal@efrei.fr'),
('Euler','Leonhard','0122456678','0699854673','0623445166','140 avenue Complexe','90000','Nanterre','leuler@efrei.fr');

INSERT INTO USER(LOGIN, PASSWORD) VALUES ("admin", "admin");
```

## Glassfish configuration (Avec JPA)

Il est necessaire de configurer un pool de connexion dans glassfish: 

1ère étape : Copier le datasource dans le répertoire bin de glassfish
```
cp bonecp-datasource.xml /opt/glassfish4/bin/
```
2ème étape : Copier le connecteur mysql dans le dossier lib de glassfish
```
cp mysql-connector-java-8.0.13.jar /opt/glassfish4/glassfish/domains/domain1/lib/
```

3ème étape : Installer le pool de connexion:
```
./asadmin
add-resources bonecp-datasource.xml
```

